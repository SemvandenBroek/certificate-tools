FROM debian:stretch

RUN apt-get -qq update
RUN apt-get install -y openssl kpcli

WORKDIR /usr/src/app

COPY ./scripts /usr/bin
RUN chmod -R +x /usr/bin

COPY ./openssl /usr/src/openssl

ENTRYPOINT /bin/bash